﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceCounter : MonoBehaviour
{
    public Transform Bike;

    private float _startPosX;
    private Text _text;
    private int _lastDistance;

    void Start()
    {
        _startPosX = Bike.position.x;
        _text = GetComponent<Text>();
    }

    void FixedUpdate()
    {
        var newDistance = Mathf.CeilToInt(Bike.position.x - _startPosX);
        if (newDistance > _lastDistance)
        {
            _text.text = $"{newDistance}m";
            _lastDistance = newDistance;
        }

    }
}
