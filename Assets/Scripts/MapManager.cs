﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapManager : MonoBehaviour
{
    public Camera MainCamera;
    public SpriteRenderer CurrentBlock;
    public SpriteRenderer BuildingBlockPrefab;

    private Vector3 _rightCornerScreenPosition;

    private void Start()
    {
        _rightCornerScreenPosition = new Vector3(Screen.width, 0f);
    }

    private void Update()
    {
        var xEdge = MainCamera.ScreenToWorldPoint(_rightCornerScreenPosition).x;
        var currentBlockExtends = CurrentBlock.bounds.center.x + CurrentBlock.bounds.extents.x;

        if (xEdge > currentBlockExtends - 2)
            SpawnNewBlock();

        if (Vector3.SqrMagnitude(CurrentBlock.transform.position - MainCamera.transform.position) > 2000f)
            SceneManager.LoadScene(0);
    }

    private void SpawnNewBlock()
    {
        var newBlock = Instantiate(BuildingBlockPrefab, transform);
        var newBlockTranform = newBlock.transform;

        var angleZ = CurrentBlock.transform.rotation.eulerAngles.z * Mathf.Deg2Rad;
        var dir = new Vector3(Mathf.Cos(angleZ), Mathf.Sin(angleZ), 0f);

        newBlockTranform.position = CurrentBlock.transform.position + dir * CurrentBlock.transform.localScale.x;// - new Vector3(0, CurrentBlock.bounds.extents);
        newBlockTranform.localScale = new Vector3(Random.Range(10f, 35f), 0.3f, 1f);
        newBlockTranform.localEulerAngles = new Vector3(0f, 0f, Random.Range(-30f, 30f));

        DeleteBlock(CurrentBlock, 10f);
        CurrentBlock = newBlock;
    }

    private IEnumerator DeleteBlock(Renderer renderer, float delay)
    {
        yield return new WaitForSeconds(delay);
        
        while (renderer.isVisible)
            yield return null;

        Destroy(renderer.gameObject);
    }
}
