﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeController : MonoBehaviour
{
    public Rigidbody2D BikeRigidbody2D;
    public float Acceleration = 800f;
    public float BrakeForce = 1500f;
    public float MinSpeed = 200f;
    public float MaxSpeed = 500f;
    public float RotationForce = 30f;

    public WheelJoint2D RearWheel;
    public WheelJoint2D FrontWheel;

    private float _currentRearSpeed;
    private float RearSpeed
    {
        set
        {
            _currentRearSpeed = value;

            if (_currentRearSpeed < -MaxSpeed)
                _currentRearSpeed = -MaxSpeed;
        }
        get => _currentRearSpeed;
    }


    private float _currentFrontalSpeed;
    private float FrontalSpeed
    {
        set
        {
            _currentFrontalSpeed = value;

            if (_currentFrontalSpeed < -MaxSpeed)
            {
                _currentFrontalSpeed = -MaxSpeed;
            }
        }
        get => _currentFrontalSpeed;
    }

    private int GetForceDirection()
    {
        return Camera.main.pixelWidth / 2 - TouchManager.GetTouchPosition().x > 0 ? 1 : -1;
    }

    private void FixedUpdate()
    {
        if (TouchManager.IsTouch())
        {
            TouchControl();
        }
        else
        {
            FrontWheel.useMotor = false;
            RearWheel.useMotor = false;
        }
    }

    private void TouchControl()
    {
        var forceDirection = GetForceDirection();

        if (!RearWheel.useMotor)
            RearSpeed = RearWheel.connectedBody.angularVelocity;

        if (forceDirection < 0)
        {
            Gas(forceDirection);
        }
        else
        {
            Brake(forceDirection);
        }

        BikeRigidbody2D.AddTorque(-forceDirection * Time.deltaTime * RotationForce);
    }

    private void Gas(int forceDirection)
    {
        RearSpeed += forceDirection * Acceleration * Time.deltaTime;
        if (RearSpeed > -MinSpeed)
            RearSpeed = -MinSpeed;
        RearWheel.motor = new JointMotor2D { motorSpeed = RearSpeed, maxMotorTorque = RearWheel.motor.maxMotorTorque };
    }

    private void Brake(int forceDirection)
    {
        if (!FrontWheel.useMotor)
            FrontalSpeed = FrontWheel.connectedBody.angularVelocity;

        var delta = forceDirection * BrakeForce * Time.deltaTime;
        RearSpeed += delta;
        FrontalSpeed += delta;

        if (RearSpeed > 0f)
            RearSpeed = 0f;

        if (FrontalSpeed > 0f)
            FrontalSpeed = 0f;

        FrontWheel.motor = new JointMotor2D { motorSpeed = FrontalSpeed, maxMotorTorque = FrontWheel.motor.maxMotorTorque };
        RearWheel.motor = new JointMotor2D { motorSpeed = RearSpeed, maxMotorTorque = RearWheel.motor.maxMotorTorque };
    }
}
