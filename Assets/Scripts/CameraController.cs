﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform Bike;
    public Vector3 CameraOffset = new Vector3(0f, 0f, -10f);

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, CameraOffset + Bike.transform.position, 0.7f);    
    }   
}
