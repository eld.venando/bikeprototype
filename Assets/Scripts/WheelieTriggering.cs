﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WheelieTriggering : MonoBehaviour
{
    public CircleCollider2D RearWheel;
    public CircleCollider2D FrontWheel;

    public float TimeToTrigger = 0.1f;
    public float ShowTime = 3f;
    public Text DisplayText;
    private bool _isWheeling;
    private bool IsWheelieMoment()
    {
        if (IsWheelConnecterToGround(FrontWheel))
            return false;
        return IsWheelConnecterToGround(RearWheel);
    }

    private bool IsWheelConnecterToGround(CircleCollider2D wheel)
    {
        Debug.DrawLine(wheel.transform.position, wheel.transform.position + Vector3.down * (wheel.radius + 1f));
        return Physics2D.CircleCast(wheel.transform.position, wheel.radius + 1f, Vector2.zero, 0f, LayerMask.GetMask("Ground"));
    }

    private void FixedUpdate()
    {
        if (IsWheelieMoment())
        {
            if (!_isWheeling)
            {
                _isWheeling = true;
                CancelInvoke("RemoveText");
                Invoke("ShowText", TimeToTrigger);
            }
        }
        else
        {
            _isWheeling = false;
            CancelInvoke("ShowText");
            Invoke("RemoveText", ShowTime);
        }
    }

    private void ShowText()
    {
        DisplayText.gameObject.SetActive(true);
    }

    private void RemoveText()
    {
        DisplayText.gameObject.SetActive(false);
    }
}
