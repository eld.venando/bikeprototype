﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TouchManager
{
    public static bool IsTouch()
    {
#if UNITY_EDITOR
        return Input.GetMouseButton(0);
#else
        return Input.touchCount > 0;
#endif
    }

    public static Vector3 GetTouchPosition()
    {
#if UNITY_EDITOR
        return Input.mousePosition;
#else
        return Input.GetTouch(0).position;
#endif
    }
}
